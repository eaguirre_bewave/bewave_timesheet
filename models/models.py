# -*- coding: utf-8 -*-
import datetime

from dateutil.relativedelta import relativedelta

from odoo import api, fields, models
from odoo import exceptions
from odoo.tools.translate import _

# ----------------------------------------------------
#  Employee allow authorize himself their timesheets
# ----------------------------------------------------
class EmployeeAllowTimesheetApproval(models.Model):
    _inherit = "hr.employee"
    timesheet_manager = fields.Boolean(string='Is Timesheet Manager')


# ----------------------------------------------------
#  Invoice adding period invoice from/to
# ----------------------------------------------------
class AccountInvoicePeriodRange(models.Model):
    _inherit = "account.invoice"
    period_from = fields.Date(string=_('Period from'))
    period_to = fields.Date(string=_('To'))
    amount_paid = fields.Float(string=_('Amount Paid'), compute='_get_amount_paid')
    invoice_project_id = fields.Many2one('project.project', string='Project')

    @api.multi
    def _get_amount_paid(self):
        for invoice in self:
            amount = 0
            if invoice.state != 'draft':
                amount = invoice.amount_total_signed - invoice.residual_signed
            invoice.update({'amount_paid': amount})


# ----------------------------------------------------
#  Sales Order adding total qty to be invoiced
# ----------------------------------------------------
class SalesOrderTotalToBeInvoiced(models.Model):
    _inherit = "sale.order"
    pending_total_qty_to_invoice = fields.Float(compute='_get_pending_total_qty_to_invoice', string="Time Bank")

    @api.multi
    def _get_pending_total_qty_to_invoice(self):
        # For each sales order

        for sales_order in self:
            product_uom_qty = 0
            qty_delivered = 0

            order_lines_ids = self.env['sale.order.line'].search([('order_id', '=', sales_order.id)])
            for line in order_lines_ids:
                product_uom_qty = product_uom_qty + line.product_uom_qty
                qty_delivered  = qty_delivered + line.qty_delivered
            total = product_uom_qty - qty_delivered
            sales_order.update({'pending_total_qty_to_invoice': total})

# ----------------------------------------------------
#  Manager Validation for timesheets
# ----------------------------------------------------
class ValidateAccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"
    manager_approved = fields.Selection([(1, _('Approved')), (0, _('Pending'))], _('Manager approval'), default=0)
    partner_approved = fields.Selection([(1, _('Approved')), (0, _('Pending')), (2, _('Rejected'))], _('Client approval'), default=0)
    is_my_manager = fields.Boolean(compute='_get_managers')

    @api.multi
    def _get_managers(self):
        current_user = self.env.user.id
        # Check if the current user has is a timesheet_manager
        current_employee = self.env['hr.employee'].search([('user_id', '=', current_user)])

        # For each timesheet item
        for timesheet in self:
            # Manager found or current user is super-user or Odoobot
            found_manager = current_employee.timesheet_manager or (current_user <= 2)
            timesheet.update({'is_my_manager': found_manager})



# ----------------------------------------------------
#  Transient model to create invoices from timesheet
# ----------------------------------------------------
class ManagerApprovalTimeSheetConfirm(models.TransientModel):
    _name = 'manager.approval.timesheet.confirm'
    title = fields.Char(default=_("Change approval status of all selected timesheets to:"))
    approval = fields.Selection([(1, _('Approved')), (0, _('Pending'))], _('Approval status'),
                                        default=0)
    @api.multi
    def confirm_approval(self):
        for item_id in self._context['active_ids']:
            timesheet_record = self.browse(item_id)
            account_analytic_line_record = self.env['account.analytic.line'].search(
                [('id', '=', timesheet_record.id)])
            if account_analytic_line_record:
                if account_analytic_line_record.is_my_manager:
                    account_analytic_line_record.write({
                        'manager_approved': self.approval
                    })
                else:
                    raise exceptions.except_orm(_('Manager Timesheet Approval Error'),
                                                _("You don't have access to execute this procedure!"))
        return True

    @api.multi
    def cancel_approval(self):
        return True

# ----------------------------------------------------
#  Transient model to create invoices from timesheet
# ----------------------------------------------------
class TimeSheetConfirmCreateInvoice(models.TransientModel):
    _name = 'timesheet.confirm.create.invoice'
    title = fields.Char(default=_("""
                            From the selected Timesheets, this process will create one invoice per Project.
                            Identical Tasks will be grouped in one invoice line.
                            To avoid duplicates, Timesheets with draft or final invoices will be ignored even if they
                            were selected by the user.                            
                                """))
    message = fields.Char(default=_('Do you want to proceed?'))

    @api.multi
    def create_invoice(self):

        # First group all timesheets per project and similar tasks
        today = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        invoices = {}
        period_from = None
        period_to = None
        # Obtain invoice periods
        for item_id in self._context['active_ids']:
            timesheet_record = self.browse(item_id)
            account_analytic_line_record = self.env['account.analytic.line'].search(
                [('id', '=', timesheet_record.id)])
            if account_analytic_line_record:
                if period_from is None or account_analytic_line_record.date <= period_from:
                    period_from = account_analytic_line_record.date
                if period_to is None or account_analytic_line_record.date >= period_to:
                    period_to = account_analytic_line_record.date

        period_from = (period_from + relativedelta(weeks=-1, weekday=-2)).strftime('%Y-%m-%d')
        period_to = (period_to + relativedelta(weeks=0, weekday=-3)).strftime('%Y-%m-%d')


        # Create invoices
        for item_id in self._context['active_ids']:
            timesheet_record = self.browse(item_id)

            account_analytic_line_record = self.env['account.analytic.line'].search(
                [('id', '=', timesheet_record.id)])
            if account_analytic_line_record:
                if not account_analytic_line_record.timesheet_invoice_id.id and account_analytic_line_record.manager_approved == 1:
                    project = str(account_analytic_line_record.project_id.id)
                    project_name = str(
                        account_analytic_line_record.project_id.name)
                    task_id = account_analytic_line_record.task_id.id
                    employee_id = account_analytic_line_record.employee_id.id or 0
                    if employee_id == 0:
                        raise exceptions.except_orm(_('Invalid timesheet'),
                                                    'There is timesheet with empty employee for project %s' % project_name)

                    employee_name = account_analytic_line_record.employee_id.name
                    journal_id = self.env['account.invoice'].default_get(['journal_id'])[
                        'journal_id']
                    if not journal_id:
                        raise exceptions.except_orm(_('Internal server error'),
                                                    _('Please define an accounting sales journal for this company.'))
                    if not invoices.get(project):
                        invoices[project] = {
                            'project_id': account_analytic_line_record.project_id.id,
                            'analytic_account': account_analytic_line_record.account_id.id,
                            'sale_order_id': None,
                            'project_name':  account_analytic_line_record.project_id.name,
                            'invoice_vals': {
                                'name': '',
                                'type': 'out_invoice',
                                'date_invoice': today,
                                'account_id': 4,
                                'partner_id': None,
                                'partner_shipping_id': None,
                                'journal_id': journal_id,
                                'currency_id': account_analytic_line_record.currency_id.id,
                                'comment': None,
                                'payment_term_id': None,
                                'fiscal_position_id': None,
                                'company_id': account_analytic_line_record.company_id.id,
                                'user_id': None,
                                'commercial_partner_id': account_analytic_line_record.partner_id.id,
                                'date': today,
                                'period_from': period_from,
                                'period_to': period_to,
                                'origin': None,
                                'date_due': today,
                                'invoice_project_id': account_analytic_line_record.project_id.id
                            },
                            'lines': {}
                        }

                    task = "%s-%s" % (str(task_id), str(employee_id))
                    if not invoices[project]['lines'].get(task):
                        invoices[project]['lines'][task] = {
                            'task_id': account_analytic_line_record.task_id.id,
                            'timesheets': [timesheet_record.id],
                            'sale_order_line_id': None,
                            'invoice_line_vals': {
                                'name': None,
                                'sequence': None,
                                'origin': None,
                                'account_id': 43,
                                'price_unit': None,
                                'quantity': account_analytic_line_record.unit_amount,
                                'discount': None,
                                'invoice_line_tax_ids': None,
                                'account_analytic_id': account_analytic_line_record.account_id.id,
                                'company_id': account_analytic_line_record.company_id.id,
                                'currency_id': account_analytic_line_record.currency_id.id,
                                'product_id': None,
                                'uom_id': None,
                                'price_unit': None,
                                'partner_id': None,
                                'invoice_id': None,
                            }
                        }
                        # Grab data from project task
                        project_task_record = self.env['project.task'].search(
                            [('id', '=', account_analytic_line_record.task_id.id)])
                        task_name = ""
                        if project_task_record:
                            task_name = project_task_record.name
                            invoices[project]['lines'][task]['invoice_line_vals']['name'] = "[%s] %s (%s)" % (
                                project_name, project_task_record.name, employee_name)
                            invoices[project]['lines'][task]['invoice_line_vals']['sequence'] = project_task_record.sequence

                            invoices[project]['lines'][task]['sale_order_line_id'] = project_task_record.sale_line_id.id

                        # Grab data from sales order line
                        current_product_id = None
                        account = 43
                        if invoices[project]['lines'][task]['sale_order_line_id'] is None:
                            invoices[project]['lines'][task]['sale_order_line_id'] = account_analytic_line_record.so_line.id

                        so_line_index = invoices[project]['lines'][task]['sale_order_line_id']
                        if not so_line_index:
                            msg = 'There is no sales order line defined for this task %s.' % task_name
                            raise exceptions.except_orm('Internal server error', msg)

                        so_line_record = self.env['sale.order.line'].search(
                            [('id', '=', so_line_index)])
                        if so_line_record:
                            current_product_id = so_line_record.product_id.id
                            invoices[project]['lines'][task]['invoice_line_vals']['product_id'] = current_product_id
                            invoices[project]['lines'][task]['invoice_line_vals']['uom_id'] = so_line_record.product_uom.id
                            invoices[project]['lines'][task]['invoice_line_vals']['price_unit'] = so_line_record.price_unit

                            account = so_line_record.product_id.property_account_income_id or so_line_record.product_id.categ_id.property_account_income_categ_id
                            if not account:
                                msg = 'Please define income account for this product: "%s" (id:%d) - or for its category: "%s".' % (so_line_record.product_id.name, so_line_record.product_id.id, so_line_record.product_id.categ_id.name)
                                raise exceptions.except_orm('Internal server error', msg)

                            fpos = so_line_record.order_id.fiscal_position_id or so_line_record.order_id.partner_id.property_account_position_id
                            if fpos:
                                account = fpos.map_account(account)
                            invoices[project]['lines'][task]['invoice_line_vals']['account_id'] = account.id
                            invoices[project]['lines'][task]['invoice_line_vals']['discount'] = so_line_record.discount
                            invoices[project]['lines'][task]['invoice_line_vals']['invoice_line_tax_ids'] = [
                                (6, 0, so_line_record.tax_id.ids)]

                        # Grab data from sales order
                        so_record = self.env['sale.order'].search(
                            [('id', '=', so_line_record.order_id.id)])
                        if so_record:
                            invoices[project]['sale_order_id'] = so_line_record.order_id.id
                            invoices[project]['invoice_vals']['origin'] = so_record.name
                            invoices[project]['invoice_vals']['partner_id'] = so_record.partner_invoice_id.id
                            invoices[project]['invoice_vals']['partner_shipping_id'] = so_record.partner_shipping_id.id
                            invoices[project]['invoice_vals']['comment'] = so_record.note

                            invoices[project]['invoice_vals']['payment_term_id'] = so_record.payment_term_id.id
                            if so_record.payment_term_id:
                                pterm = so_record.payment_term_id
                                pterm_list = pterm.with_context(currency_id=invoices[project]['invoice_vals']['currency_id']).compute(value=1, date_ref=today)[0]
                                invoices[project]['invoice_vals']['date_due'] = max(line[0] for line in pterm_list)

                            invoices[project]['invoice_vals']['fiscal_position_id'] = so_record.fiscal_position_id.id or so_record.partner_invoice_id.property_account_position_id.id

                            invoices[project]['lines'][task]['invoice_line_vals']['origin'] = so_record.name
                            invoices[project]['lines'][task]['invoice_line_vals']['partner_id'] = so_record.partner_invoice_id.id

                            # Set sales person from client
                            partner_record = self.env['res.partner'].search([('id', '=', so_record.partner_invoice_id.id)])
                            if partner_record:
                                if partner_record.user_id:
                                    invoices[project]['invoice_vals']['user_id'] = partner_record.user_id.id
                    else:
                        invoices[project]['lines'][task]['timesheets'].append(
                            timesheet_record.id)
                        invoices[project]['lines'][task]['invoice_line_vals']['quantity'] = \
                            invoices[project]['lines'][task]['invoice_line_vals']['quantity'] + \
                            account_analytic_line_record.unit_amount
                else:
                    print('Timesheet record (id = %s) skipped it already has a draft or valid invoice or not approved.' %
                          timesheet_record.id)
            else:
                raise exceptions.except_orm('Timesheet Invoice Create Server Error',
                                            'Could no access data for timesheet record (id = %s).' % timesheet_record.id)


        # Validate that all invoices have a valid sales order
        success = True
        for _, invoice in invoices.items():
            if invoice:
                if invoice['sale_order_id'] is None:
                    success = False
                    msg = "Error! Project name : %s does not have a sales order defined. " % invoice['project_name']
                    print(msg)
                    raise exceptions.UserError(msg)

        # Create invoice with the active ids
        if success:
            for _, invoice in invoices.items():

                if invoice:
                    project_id = invoice['project_id']
                    # create new record
                    invoice_record = self.env['account.invoice'].create(
                        invoice['invoice_vals'])
                    if invoice_record:
                        invoice_id = invoice_record['id']
                        self.env.cr.execute("""
                                            UPDATE sale_order 
                                            SET analytic_account_id=%s WHERE id=%s
                                            """ % (invoice['analytic_account'], invoice['sale_order_id']))

                        for _, line in invoice['lines'].items():
                            if line:
                                line['invoice_line_vals']['invoice_id'] = invoice_id
                                task_id = line['task_id']
                                # Create invoice line
                                invoice_line_record = self.env['account.invoice.line'].create(
                                    line['invoice_line_vals'])
                                # Update timesheet line to set the invoice id
                                if invoice_line_record and invoice_id:
                                    invoice_line_id = invoice_line_record['id']

                                    self.env.cr.execute("""
                                                        INSERT INTO sale_order_line_invoice_rel
                                                        VALUES (%s,%s)
                                                        """ % (invoice_line_id, line['sale_order_line_id']))

                                    self.env.cr.execute("""
                                                        UPDATE account_analytic_line 
                                                        SET timesheet_invoice_id=%s 
                                                        WHERE id in (%s)  
                                                        """ % (invoice_id, str(line['timesheets']).strip('[]')))

                                    self.env.cr.execute("""
                                                UPDATE sale_order_line 
                                                SET task_id=%s
                                                WHERE id = %s
                                                """ % (line['task_id'],
                                                       line['sale_order_line_id']))

                        invoice_record.compute_taxes()

        return True

    @api.multi
    def cancel_create_invoice(self):
        return True

