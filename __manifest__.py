# -*- coding: utf-8 -*-
{
    'name': "Bewave Timesheet to Invoice",
    'summary': """
        Select timesheet records that will be invoiced
            """,
    'author': "Bewave Technologies Inc.",
    'website': "http://www.bewave.io",
    'category': 'Bewave',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base','account', 'sale', 'hr_timesheet'],

    # always loaded
    'data': [
        'views/templates.xml',
        'views/views.xml'
    ],
}
